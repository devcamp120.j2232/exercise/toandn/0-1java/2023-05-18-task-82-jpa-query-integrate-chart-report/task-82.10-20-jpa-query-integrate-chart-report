package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.repository.IEmployeeRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
  @Autowired
  IEmployeeRepository gIEmployeeRepository;

  public ArrayList<Employee> getAllEmployee() {
    ArrayList<Employee> listEmployee = new ArrayList<>();
    gIEmployeeRepository.findAll().forEach(listEmployee::add);
    return listEmployee;
  }

  public Employee createEmployee(Employee pEmployee) {
    try {
      Employee vEmployeeSave = gIEmployeeRepository.save(pEmployee);
      return vEmployeeSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Employee updateEmployee(Employee pEmployee, Optional<Employee> pEmployeeData) {
    try {
      Employee vEmployee = pEmployeeData.get();
      vEmployee.setLastName(pEmployee.getLastName());
      vEmployee.setFirstName(pEmployee.getFirstName());
      vEmployee.setExtension(pEmployee.getExtension());
      vEmployee.setEmail(pEmployee.getEmail());
      vEmployee.setOfficeCode(pEmployee.getOfficeCode());
      vEmployee.setReportTo(pEmployee.getReportTo());
      vEmployee.setJobTitle(pEmployee.getJobTitle());
      Employee vEmployeeSave = gIEmployeeRepository.save(vEmployee);
      return vEmployeeSave;
    } catch (Exception e) {
      return null;
    }
  }
}
