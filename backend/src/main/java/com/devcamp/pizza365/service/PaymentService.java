package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.IPaymentRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
  @Autowired
  IPaymentRepository gIPaymentRepository;

  public ArrayList<Payment> getAllPayment() {
    ArrayList<Payment> listPayment = new ArrayList<>();
    gIPaymentRepository.findAll().forEach(listPayment::add);
    return listPayment;
  }

  public Payment createPayment(Payment pPayment,
      Optional<Customer> pCustomerData) {
    try {
      Payment vPayment = new Payment();
      vPayment.setCheckNumber(pPayment.getCheckNumber());
      vPayment.setAmmount(pPayment.getAmmount());
      vPayment.setCustomer(pCustomerData.get());
      Payment vPaymentSave = gIPaymentRepository.save(vPayment);
      return vPaymentSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Payment updatePayment(Payment pPayment, Optional<Payment> pPaymentData,
      Optional<Customer> pCustomerData) {
    try {
      Payment vPayment = pPaymentData.get();
      vPayment.setCheckNumber(pPayment.getCheckNumber());
      vPayment.setAmmount(pPayment.getAmmount());
      vPayment.setCustomer(pCustomerData.get());
      Payment vPaymentSave = gIPaymentRepository.save(vPayment);
      return vPaymentSave;
    } catch (Exception e) {
      return null;
    }
  }

}
